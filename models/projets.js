let connection = require('../config/db')
let moment = require('../config/moment-FR')

class Projets {

    constructor (row) {
        this.row = row
    }
   
    static all (cb) {
        connection.query('SELECT * FROM projets ORDER by created_at DESC', (err, rows) => {
            if (err) throw err
            cb(rows)
        })
    }
}

module.exports = Projets
