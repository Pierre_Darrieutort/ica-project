let express = require('express')
let app = express()
const Projets = require('./models/projets')


//Moteur de templates
app.set('view engine', 'ejs')


//Middleware
app.use('/assets', express.static('public'))


//Routes
app.get('/', (req, res) => {
    res.render('pages/accueil', {navigation : 'accueil'})
})

app.get('/projets', (req, res) => {
    Projets.all((result) => {
        res.render('pages/projets', {listeProjets: result, navigation : 'projets'})
    })
})

app.get('/profil', (req, res) => {
    res.render('pages/profil', {navigation : 'profil'})
})


app.listen(8080)
